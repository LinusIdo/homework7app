var express = require('express');
var router = express.Router();
var crypto=require('crypto');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', { title: 'Express'});
});
function decrypt(text){
  var decipher=crypto.createDecipher('aes256','asaadsaad');
  var dec=decipher.update(text,'hex','utf8');
  dec+=decipher.final('utf8');
  return dec;
}
router.get('/message',function(req,res){
  var db=req.db;
  var crypto=req.crypto;
  var collection=db.collection('homework7');
  collection.findOne({},{}, function (err, doc) {
    if (err) throw err;
    var encrypted="ba12e76147f0f251b3a2975f7acaf446a86be1b4e2a67a5d51d62f7bfbed5c03";
    var decrypted =decrypt(doc.message); 
    res.end('Encrypted Message:'+encrypted+'\nDecrypted Text is:'+decrypted);
  });
});
module.exports = router;
